factorial n = product [1..toInteger n]
fallingFactorial x n = product [x - i | i <- [0..fromIntegral n - 1] ]
choose n k = fallingFactorial n k `div` factorial k
fun n=(2 ^ n - n - 1) * choose 26 n 
euler_158=maximum$map fun [1..26]