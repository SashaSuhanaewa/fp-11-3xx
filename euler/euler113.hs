module Euler113 where

choose :: Integral a => a -> a -> a
choose m n = product [(n+1)..m] `div` product [2..(m-n)]

notBouncyCount :: Integral a => a -> a
notBouncyCount n = choose (n+10) 10 + choose (n+9) 9 - 2 - 10 * n

euler_113 = notBouncyCount 100