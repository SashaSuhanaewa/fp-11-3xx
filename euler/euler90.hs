options = [[a,b,c,d,e,f] | a <- [0..4], b <-[(a+1)..5], c<-[(b+1)..6], d<-[(c+1)..7], e<-[(d+1)..8], f<-[(e+1)..9]]
options' = map add6 $ map add9 options
  where add6 xs = if (9 `elem` xs && not (6 `elem` xs)) then 6:xs else xs
        add9 xs = if (6 `elem` xs && not (9 `elem` xs)) then 9:xs else xs
goodPair xs ys = (check xs ys 0 1) && (check xs ys 0 4) && (check xs ys 0 9) && (check xs ys 1 6) && (check xs ys 2 5) &&
                 (check xs ys 3 6) && (check xs ys 4 9) && (check xs ys 6 4) && (check xs ys 8 1)
check xs ys x y = (x `elem` xs && y `elem` ys) || (x `elem` ys && y `elem` xs)
euler_90 = sum [1 | d1 <- options', d2 <- dropWhile (/= d1) options', goodPair d1 d2]
