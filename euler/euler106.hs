binomial x y =(prodxy (y+1) x) `div` (prodxy 1 (x-y))
prodxy x y=product[x..y]
catalan n=(`div` (n+1)) $binomial (2*n) n
calc n=
    sum[e*(c-d)|
    a<-[1..di2],
    let mu2=a*2,
    let c=(`div` 2) $ binomial mu2 a,
    let d=catalan a,
    let e=binomial n mu2]
    where
    di2=n `div` 2
euler_106 = calc 12