import Data.List
import Data.Ratio
-- Continued fraction representations of square roots are periodic
-- Here we calculate the periodic expansion
squareRootPeriod :: Int -> Int -> Int -> [Int]
squareRootPeriod r n d = m : rest
    where
    m = (truncate (sqrt (fromIntegral r)) + n) `div` d
    a = n - d * m
    rest | d == 1 && n /= 0 = []
         | otherwise = squareRootPeriod r (-a) ((r - a ^ 2) `div` d)
 
-- Turn the period into an infinite stream
continuedFraction :: [Int] -> [Integer]
continuedFraction (x:xs) = map fromIntegral $ x : cycle xs
 
-- calculate successive convergents as a ratio  
convergents ::  [Integer] -> [Ratio Integer]
convergents l = zipWith (%) (drop 2 $ hn) (drop 2 $ kn)
  where 
    hn = 0:1:zipWith3 (\x y z -> x*y+z) l (tail hn) hn
    kn = 1:0:zipWith3 (\x y z -> x*y+z) l (tail kn) kn
 
-- here are the guts of the solution
-- we calculate convergents until the size of the denominator exceeds
-- the given bound.  This is one candidate for the closest rational 
-- approximation.  The other candidate is a semiconvergent, which is
-- calculated as p3%q3
closest2 :: Integer -> Integer -> [Ratio Integer]
closest2 bound n = [p,(p3%q3)]
  where
      a = convergents $ continuedFraction $ squareRootPeriod (fromIntegral n) 0 1
      b = takeWhile ((<= bound) . denominator) a
      c = reverse b
      (p:q:_) = c
      (p1,q1) = (numerator p, denominator p)
      (p2,q2) = (numerator q, denominator q)
      p3 = ((bound-q2) `div` q1) * p1 + p2
      q3 = ((bound-q2) `div` q1) * q1 + q2
 
-- pick the ratio returned from closest2 which is
-- actually closer to the square root, and return the denominator
denomClosest :: Integer -> Integer -> Integer      
denomClosest bound n = denominator c1
  where [l,r] = closest2 bound n
        c1 | abs (l*l - (n%1)) < abs (r*r - (n%1)) = l
           | otherwise                             = r
 
isSquare :: Integer -> Bool
isSquare n = n `elem` takeWhile (<= n) [n*n | n <- [x..] ]
  where x = floor . sqrt . fromIntegral $ n
 
nonSquares :: Integer -> [Integer]
nonSquares k = [ n | n<-[2..k] , (not . isSquare) n]
 
bound = (10^12)
 
euler_192 :: Integer
euler_192 =
  sum $ map (denomClosest bound) (nonSquares 100000)